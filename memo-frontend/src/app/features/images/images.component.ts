import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { concatMap } from 'rxjs';

import { Image, ImageResponse } from 'src/app/services/image/image.interface';
import { ImageService } from 'src/app/services/image/image.service';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css'],
})
export class ImagesComponent implements OnInit {
  images: Image[] = [];
  filteredImages: Image[] = [];
  imagesSlice: Image[] = [];

  length = 0;
  pageSizeOptions = [3, 6, 9];

  constructor(private imageService: ImageService, private router: Router) {}

  ngOnInit(): void {
    const observer = {
      next: (image: ImageResponse) => {
        this.images.push(this.imageService.convertToImage(image));
        this.images.sort((a, b) => a.name.localeCompare(b.name));
        this.filteredImages.push(this.imageService.convertToImage(image));
        this.filteredImages.sort((a, b) => a.name.localeCompare(b.name));
        this.length = this.filteredImages.length;
        this.imagesSlice = this.filteredImages.slice(0, 3);
      },
      error: (err: HttpErrorResponse) => {
        console.log(err);
      },
    };

    this.imageService
      .getImages()
      .pipe(
        concatMap((images) =>
          images.map((image) => this.imageService.getImage(image.id))
        ),
        concatMap((image) => image)
      )
      .subscribe(observer);
  }

  navToDetail(id: number) {
    this.router.navigate(['/draw', { id: id }]);
  }

  handlePageEvent(e: PageEvent) {
    const startIndex = e.pageIndex * e.pageSize;
    let endIndex = startIndex + e.pageSize;

    if (endIndex > this.filteredImages.length) {
      endIndex = this.filteredImages.length;
    }
    this.imagesSlice = this.filteredImages.slice(startIndex, endIndex);
  }

  filterImages(e: any) {
    this.filteredImages = this.images.filter((image) =>
      image.name.toLowerCase().startsWith(e.target.value.toLowerCase())
    );
    this.imagesSlice = this.filteredImages.slice(0, 3);
    this.length = this.filteredImages.length;
  }
}
