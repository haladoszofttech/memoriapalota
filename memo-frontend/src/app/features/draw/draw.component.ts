import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { saveAs } from 'file-saver';
import { faTrash, faDownload } from '@fortawesome/free-solid-svg-icons';

import { ImageResponse } from 'src/app/services/image/image.interface';
import { ImageService } from 'src/app/services/image/image.service';

@Component({
  selector: 'app-draw',
  templateUrl: './draw.component.html',
  styleUrls: ['./draw.component.css'],
})
export class DrawComponent implements OnInit {
  source?: string;
  imageId: number = 0;
  fileName: string = '';
  testImg = new Image();

  deleteIcon = faTrash;
  downloadIcon = faDownload;

  // width = 616;
  // height = 742;
  width = -1;
  height = -1;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private imageService: ImageService,
    private ngZone: NgZone
  ) {}

  ngOnInit(): void {
    this.imageId = Number(this.route.snapshot.paramMap.get('id'));

    const observer = {
      next: (image: ImageResponse) => {
        this.source = `data:image/jpg;base64,${image.content}`;
        this.testImg.src = this.source;
        this.testImg.addEventListener('load', () => {
          this.width = Math.floor(
            Math.min(
              this.testImg.width *
                (window.innerWidth / this.testImg.width) *
                0.9,
              this.testImg.width * (1000 / this.testImg.height) * 0.9
            )
          );
          this.height = Math.floor(
            Math.min(
              this.testImg.height *
                (window.innerWidth / this.testImg.width) *
                0.9,
              this.testImg.height * (1000 / this.testImg.height) * 0.9
            )
          );
        });
        this.fileName = image.fileName;
      },
      error: (err: HttpErrorResponse) => {
        this.ngZone.run(() => {
          this.router.navigate(['/images']);
        });
      },
    };
    this.imageService.getImage(this.imageId).subscribe(observer);
  }

  save(event: Blob) {
    if (this.imageId === 0) {
      this.ngZone.run(() => {
        this.router.navigate(['/images']);
      });
      return;
    }
    const formData = new FormData();
    formData.append('file', new File([event], this.fileName));

    this.imageService.postImage(formData).subscribe((next) => {
      return;
    });

    const observer = {
      complete: () => {
        this.ngZone.run(() => {
          this.router.navigate(['/images']);
        });
      },
    };
    this.imageService.deleteImage(this.imageId).subscribe(observer);
  }

  cancel() {
    this.ngZone.run(() => {
      this.router.navigate(['/images']);
    });
  }

  delete() {
    if (this.imageId === 0) {
      this.ngZone.run(() => {
        this.router.navigate(['/images']);
      });
      return;
    }
    const observer = {
      complete: () => {
        this.ngZone.run(() => {
          this.router.navigate(['/images']);
        });
      },
    };
    this.imageService.deleteImage(this.imageId).subscribe(observer);
  }

  download() {
    if (this.imageId === 0) {
      this.ngZone.run(() => {
        this.router.navigate(['/images']);
      });
      return;
    }
    saveAs(this.source!, this.fileName);
  }
}
