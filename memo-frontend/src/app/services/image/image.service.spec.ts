import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { ImageService } from './image.service';

describe('ImageService', () => {
  let service: ImageService;
  let httpClientMock: any;

  beforeEach(() => {
    httpClientMock = {
      get: jest.fn(),
      request: jest.fn(),
      delete: jest.fn(),
    };

    TestBed.configureTestingModule({
      providers: [
        ImageService,
        { provide: HttpClient, useValue: httpClientMock },
      ],
    });
    service = TestBed.inject(ImageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return http response in getImage', () => {
    const expValue = { id: 1, fileName: 'test.jpg', content: 'test' };
    const getSpy = jest
      .spyOn(httpClientMock, 'get')
      .mockReturnValue(of(expValue));
    service.getImage(expValue.id).subscribe((next) => {
      expect(next).toBe(expValue);
    });

    expect(getSpy).toBeCalledWith(
      'https://api-memoriapalota.g14.hu/memo/image/1'
    );
    expect(getSpy).toBeCalled();
  });

  it('should return http response in getImages', () => {
    const expValue = [
      {
        id: 1,
        name: 'test.jpg',
      },
      {
        id: 2,
        name: 'test2.jpg',
      },
    ];
    const getSpy = jest
      .spyOn(httpClientMock, 'get')
      .mockReturnValue(of(expValue));
    service.getImages().subscribe((next) => {
      expect(next).toBe(expValue);
    });

    expect(getSpy).toBeCalledWith(
      'https://api-memoriapalota.g14.hu/memo/image'
    );
    expect(getSpy).toBeCalled();
  });

  it('postImage should call http request', () => {
    const requestSpy = jest.spyOn(httpClientMock, 'request');
    service.postImage(null);
    expect(requestSpy).toBeCalled();
  });

  it('deleteImage should call http delete', () => {
    const deleteSpy = jest.spyOn(httpClientMock, 'delete');
    service.deleteImage(666);
    expect(deleteSpy).toBeCalled();
    expect(deleteSpy).toBeCalledWith(
      'https://api-memoriapalota.g14.hu/memo/image/666'
    );
  });
});
